// Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.

// Для цього існує два методи:

// 1. document.createElement('tagName');
// Створює елемент з іменем tagName.

// 2. document.createTextNode('text')
// Створює новий текстовий вузол із заданим текстом.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі
// варіанти цього параметра.

// insertAdjacentHTML(where, html) - метод, що дає можливість вставити HTML повністю, з усіма тегами та текстом.
// Перший параметр це кодове слово, яке вказує куди вставляти відносно elem.
// Його значення має бути одним з наступних:
// "beforebegin" – вставити html безпосередньо перед elem,
// "afterbegin" – вставити html в elem, на початку,
// "beforeend" – вставити html в elem, в кінці,
// "afterend" – вставити html безпосередньо після elem.

// Другим параметром є рядок HTML, який вставляється “як HTML”.

// 3. Як можна видалити елемент зі сторінки?

// Для цього використовується метод elem.remove();


//     Завдання

/*Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді 
списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу 
jQuery або React.

Технічні вимоги:
- Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - 
DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.)
- кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

["1", "2", "3", "sea", "user", 23];

Можна взяти будь-який інший масив.
Необов'язкове завдання підвищеної складності:
Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, 
виводити його як вкладений список. Приклад такого масиву:

["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) 
перед очищенням сторінки.*/

const arrayExample = [
  "hello",
  "world",
  "Kiev",
  ["Borispol", "Irpin"],
  "Kharkiv",
  "Odessa",
  "Lviv",
];

function arrayList(array, parent = document.body) {
  const ulElement = document.createElement("ul");
  parent.append(ulElement);

  array.map((element) => {
    const liElement = document.createElement("li");

    if (Array.isArray(element)) {
      arrayList(element, liElement);
    } else {
      liElement.innerHTML = element;
    }
    ulElement.append(liElement);
  });
};

arrayList(arrayExample, (parent = document.body));

/* ----- Функція для очистки ----- */

function clear() {
  document.querySelector("body").innerHTML = "";
}

/* ----- Таймер зворотнього відліку ----- */

function counter(sec) {
  if (sec >= 0) {
    console.log(`${sec}c`);
    setTimeout(() => {
      counter(sec - 1);
    }, 1000);
  }
  setTimeout(clear, 3000);
}
counter(3);
